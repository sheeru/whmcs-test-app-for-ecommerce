<?php
/**
 * Singleton Pattern example in PHP
 * http://AdamScheller.com/
 *
 */
class UserInfo {
    private static $_singleton;

    private function __construct() {
    }
 
    public static function getInstance() {
        if(!self::$_singleton) {
            self::$_singleton = new UserInfo();
        }
        return self::$_singleton;
    }

    public function ValidateLogin($username,$password){
	$cl_username = $_POST["username"];
	$cl_password = $_POST["password"];


	$url = "http://localhost/whmcs/includes/api.php"; # URL to WHMCS API file
	$username = "admin";
	$password = "admin";

	$postfields["username"] = $username;
	$postfields["password"] = md5($password);
	$postfields["action"] = "validatelogin";
	$postfields["email"] = $cl_username;
	$postfields["password2"] = $cl_password;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 100);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	$data = curl_exec($ch);
	curl_close($ch);

	$data = explode(";",$data);
	foreach ($data AS $temp) {
	     $temp1 = explode("=",$temp);
	     if(count($temp1)>=2){
		     $results[$temp1[0]] = $temp1[1];
	     }
	}
	   
	if ($results["result"]=="success") {
		$this->addToLoggedinUsersList($username);
		return("success");
	} 
	else {
		# An error occured
		return("The following error occured: ".$results["message"]);
	}
    }
 
}
 
?>
