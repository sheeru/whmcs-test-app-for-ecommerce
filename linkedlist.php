<?php

class ListNode{
	private $data;
	private $next;

	function __construct($data){
		$this->data = $data;
		$this->next = NULL;
	}

	function getData(){
		return $this->data;
	}
	
	function setData($data){
		$this->data = $data;
	}
}

class List{
	private $head;
	private $tail;
	private $size;

	function __construct(){
		$this->head = NULL;
		$this->tail = NULL;
		$this->size = 0;
	}

	public function append($data){
		if($head == NULL){
			$temp = new ListNode($data);
			$this->head = &$temp;
			$this->tail = &$temp;
		}
		else{
			$temp = new ListNode($data);
			$this->tail->next = $temp;
			$this->tail = &$temp;
			$this->size++;						
		}
	}

	public function delete($data){
		$temp = $this->head;
		if($temp == NULL)
			return -1;
		while($temp != NULL && strcmp($data,$temp->data)){
			$temp = $temp->next;
		}
		if ($temp == NULL)
			return -1;
		else{
			$temp->data = $temp->next->data;
			$temp->next = $temp->next->next;	
		}
			
	}

}		
 
?>
