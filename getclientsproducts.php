<?php
 /* *** WHMCS XML API Sample Code *** */
 
function getclientsproducts($clientid)
{
 $url = "http://localhost/whmcs/includes/api.php"; # URL to WHMCS API file goes here
 $username = "admin"; # Admin username goes here
 $password = "admin"; # Admin password goes here
 
 $postfields = array();
 $postfields["username"] = $username;
 $postfields["password"] = md5($password);
 $postfields["action"] = "getclientsproducts";
 $postfields["clientid"] = $clientid;
 $postfields["responsetype"] = "xml";
 
 $query_string = "";
 foreach ($postfields AS $k=>$v) $query_string .= "$k=".urlencode($v)."&";
 
 $ch = curl_init();
 curl_setopt($ch, CURLOPT_URL, $url);
 curl_setopt($ch, CURLOPT_POST, 1);
 curl_setopt($ch, CURLOPT_TIMEOUT, 30);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 $xml = curl_exec($ch);
 if (curl_error($ch) || !$xml) $xml = '<whmcsapi><result>error</result>'.
 '<message>Connection Error</message><curlerror>'.
 curl_errno($ch).' - '.curl_error($ch).'</curlerror></whmcsapi>';
 curl_close($ch);
 
 $arr = whmcsapi_xml_parser($xml); # Parse XML
 
 return($arr); # Output XML Response as Array
} 
 /*
 Debug Output - Uncomment if needed to troubleshoot problems 
 echo "<textarea rows=50 cols=100>Request: ".print_r($postfields,true);
 echo "\nResponse: ".htmlentities($xml)."\n\nArray: ".print_r($arr,true);
 echo "</textarea>";
 */
 

 
 ?>
