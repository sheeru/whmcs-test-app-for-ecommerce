
<html>                                                                       
	<head>                                                                      
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">     
		<title>Login Page</title>  
		<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
			    
			    $('#login').click(function() {
				$.ajax({
				    type: "POST",
				    url: 'logincheck.php',
				    data: {
					username : $("#username").val(),
					password : $("#password").val()
				    },
				    success: function(data)
				    {
					if (data === 'Correct') {
						alert('LOGIN CORRECT');
						location.reload();

					}
					else {
					    alert(data);
			
					}
				   }
				});

			    });


			    $('#logout').click(function() {
				$.ajax({
				    type: "POST",
				    url: 'logout.php',

				    success: function(data)
				    {
					if (data === 'Correct') {
						alert('LOGGED OUT SUCCESSFULLY');
						location.reload();

					}
					else {
					    alert(data);
			
					}
				   }
				});

			    });


			});    
	</script>                                                                  
		</head>                                                   

	<body>
	<?php
	session_name("urandomlogin");
	session_start();

	require_once('xmlparser.php');
	require_once('getclientsproducts.php');
	require_once('getclients.php');
	require_once('getorder.php');

	if(!isset($_SESSION['loggedin']))
	{?>
		<div id="loginform">
			Username:<input type="text" id="username"/></br>
			Password:<input type="password" id="password"/></br>
			<input type="button" id="login" value="Login"/>
		</div>	
	<?php }else{ 
		echo "Hi, ".$_SESSION['username']; ?></br></br>
	<?php
		$clientid = getclientid();
		$clientproducts = getclientsproducts($clientid);

		$cltpdtarr = array();
		$cltpdtarr_pid = array();
		$pend_invoiceid = array();

		/*TODO: Filter the pending items alone with order status and store it in unqcltpdtarr*/
		foreach($clientproducts['WHMCSAPI']['PRODUCTS'] as $products)
		{

			$orderstatus = getorder($clientid,$products["ORDERID"]);
			if($orderstatus['WHMCSAPI']['ORDERS']['ORDER']['STATUS'] == "Pending")
			{
				$cltpdtarr[] = array("PID"=>$products["PID"],
									"NAME"=>$products["NAME"],
									"GROUPNAME"=>$products["GROUPNAME"],
									"RECURRINGAMOUNT"=>$products["RECURRINGAMOUNT"]);

/*				$cltpdtarr_pid_oid[] = array("PID"=>$products["PID"],
									"ORDERID"=>$products["ORDERID"]);
*/
				$cltpdtarr_pid[] = $products["PID"];

				$pend_invoiceid[] = array("amount"=>$products["RECURRINGAMOUNT"],
										  "invoiceid"=>$orderstatus['WHMCSAPI']['ORDERS']['ORDER']['INVOICEID'],
										  "orderid"=>$products["ORDERID"]);
			}


		}
		$unqcltpdtarr = array_unique($cltpdtarr,SORT_REGULAR);
		$cltpdtarr_pid_cnt = array_count_values($cltpdtarr_pid);
		$_SESSION['pendingtrans'] = $pend_invoiceid;

		$i = 1;
	?>
		<div id="cartitems">
	<?php

		foreach($unqcltpdtarr as $unqcltpdt)
		{?>
			<div id="<? echo 'r'.$i ?>">
			<div id="<? echo 'r'.$i.'SNo'?>"><? echo $i ?> </div>
			<div id="<? echo 'r'.$i.'Name'?>"> <? echo $unqcltpdt['NAME'] ?> </div>
			<div id="<? echo 'r'.$i.'GroupName' ?>"> <? echo $unqcltpdt['GROUPNAME'] ?> </div>
			<div id="<? echo 'r'.$i.'Qty' ?>"> <? echo $cltpdtarr_pid_cnt[$unqcltpdt['PID']] ?> </div>
			<div id="<? echo 'r'.$i.'EachAmt' ?>"> <? echo $unqcltpdt['RECURRINGAMOUNT'] ?> </div>
			<div id="<? echo 'r'.$i.'TotAmt' ?>"> <? echo $cltpdtarr_pid_cnt[$unqcltpdt['PID']]*$unqcltpdt['RECURRINGAMOUNT'] ?> </div>
			</div>
			<? $i++;		
		}?>
	
		</div>
		<input type="button" value="checkout" id="checkout" onClick="window.location.href='checkout.php'"></input>
		<div id="logout">Logout</div>
	<?php } ?>
	</body>
</html>
